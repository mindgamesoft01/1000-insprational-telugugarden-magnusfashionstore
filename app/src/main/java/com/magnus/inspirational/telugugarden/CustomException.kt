package com.magnus.inspirational.telugugarden

class CustomException(message: String) : Exception(message)
