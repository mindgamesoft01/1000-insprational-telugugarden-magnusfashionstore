package com.magnus.inspirational.telugugarden

interface AppInterfaces {
    fun loadItem()
    fun loadSplashScreen()
    fun loadStartScreen()
    fun loadImageTopics()
    fun loadMenus()
    fun loadTestModeScreen()
    fun loadBookMarkMenu()
    fun loadBookMarkItem()
    fun loadPrivacyPolicy()


}